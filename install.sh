#!/usr/bin/env sh
mkdir env
virtualenv -p $(which python3) env
env/bin/pip install PyQt5
env/bin/pip install pandas
