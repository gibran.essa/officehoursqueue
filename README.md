# README
## INFO
creates a queue for use in GT TA office hours

to use, download the gatech roster from canvas, convert to a csv

pressing "Add" will allow either a name entry or buzzcard swipe.
pressing "Remove from top" will ask for a password, that TAs should know, and if that is correct will remove from the top of the queue.
pressing "Clear" empties the queue.

## Installation
### With a Virtual Environment
install https://virtualenv.pypa.io/en/stable/

run `./install.sh`

to launch:
run `./run.sh [roster.csv] [password]`

### Without a Virtual Environment

use python3
install PyQt5 and pandas

run `python /src/gui.py [roster.csv] [password]`







written by Gibran Essa and Farhan Tejani
