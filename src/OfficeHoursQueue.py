from collections import deque
import pandas as pd

class OfficeHoursQueue():
    def __init__(self, roster):
        self.roster = roster
        self.student_map = self.load_roster(self.roster)
        self.q = deque()

    @staticmethod
    def load_roster(file):
        d = {}
        df = pd.read_csv(file)
        #df = df.loc[df['Role'] == 'Student']
        for i, row in df.iterrows():
            d[row['GTID']] = row['Name']
        return d

    def swipe_to_student(self, swipe):
        start_ind = swipe.find('1570') + 5
        gtid = int(swipe[start_ind:start_ind + 9])
        return self.student_map[gtid]

    def add(self, swipe):
        try:
            student = self.swipe_to_student(swipe)
        except KeyError as e:
            return False
        if self.q.count(student) > 0:
            return False
        self.q.append(student)
        return True

    def remove(self):
        return self.q.popleft()

    def remove(self, i):
        del self.q[i]

    def __str__(self):
        return self.q.__str__()

    def __len__(self):
        return len(self.q)


if __name__ == '__main__':
    q = OfficeHoursQueue('Roster.csv')
    while True:
        swipe = input("Swipe your Buzzcard\n")
        q.add(swipe)
        print(q)
