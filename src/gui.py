from OfficeHoursQueue import OfficeHoursQueue as OH


import sys
from PyQt5.QtWidgets import QApplication, QWidget, QListWidget, QPushButton, QGridLayout, QInputDialog, QLineEdit

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont



class QueueWindow(QWidget):

    def __init__(self, roster):
        super().__init__()
        self.helper = OH(roster)
        self.queue = QListWidget()
        self.rem = QPushButton('Remove From Top')
        self.add = QPushButton('Add Yourself')
        self.cls = QPushButton('Clear')
        self.add2 = QPushButton('Add To Top')
        self.layout = QGridLayout(self)
        self.initLayout()
        self.initUI()



    def initLayout(self):
        self.layout.addWidget(self.queue, 0, 0, 9, 1)
        self.layout.addWidget(self.rem, 0, 1)
        self.layout.addWidget(self.add, 1, 1)
        self.layout.addWidget(self.cls, 2, 1)
        self.layout.addWidget(self.add2, 3, 1)

    def initUI(self):
        self.setWindowTitle('Queue')
        self.showFullScreen()
        self.cls.clicked.connect(self.clear)
        self.rem.clicked.connect(self.remove)
        self.add.clicked.connect(self.addf)
        self.add2.clicked.connect(self.addTop)
        self.queue.setFont(QFont("Courier", 25))

    def remove(self):
        text, okPressed = QInputDialog.getText(self, 'Get text','Input Password:', QLineEdit.Password, '')
        if not okPressed:
            return
        if text != sys.argv[2]:
            return
        self.queue.takeItem(0)

    def addDialog(self):
        text, okPressed = QInputDialog.getText(self, 'Get text','Swipe Card or enter Name:', QLineEdit.Normal, '')
        if not okPressed:
            return 'exit'
        return text

    def addf(self):
        text = self.addDialog()
        if text == 'exit':
            return
        try:
            text = self.helper.swipe_to_student(text) if text[0] in '1234567890;' else text
        except KeyError:
            return
        self.queue.insertItem(self.queue.count(), text)

    def addTop(self):
        text = self.addDialog()
        if text == 'exit':
            return
        try:
            text = self.helper.swipe_to_student(text) if text[0] in '1234567890;' else text
        except KeyError:
            return
        passwd, okPressed = QInputDialog.getText(self, 'Get text','Input Password:', QLineEdit.Password, '')
        if not okPressed:
            return
        if passwd != sys.argv[2]:
            return
        self.queue.insertItem(0, text)


    def clear(self):
        passwd, okPressed = QInputDialog.getText(self, 'Get text','Input Password:', QLineEdit.Password, '')
        if not okPressed:
            return
        if passwd != sys.argv[2]:
            return
        self.queue.clear()





if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = QueueWindow(sys.argv[1])
    sys.exit(app.exec_())
